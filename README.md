# NSWI200 Operating Systems Course Toolchain

This repository hosts scripts for building toolchain for the
[Operating Systems course (NSWI200)](https://d3s.mff.cuni.cz/teaching/nswi200/)
at [Charles University](https://cuni.cz/).

We build toolchain for MIPS 32 and RISC-V 32.

The following scripts (configurations) are available:

 * shell scripts to build the toolchain from sources
 * RPM files that are used by Fedora COPR
 * Docker files for building images with the toolchain and other tools

## Maintenance notes

When upgrading to a newer version of the toolchain:

1. Update the versions (and MD5 sums) in the `from-sources/*/build-*.sh`
   scripts.
2. Run the above build scripts and check that `from-sources/*/tmp-*/PKG/`
   contains valid installation.
3. Update the `from-sources/*/.fpm` files (version numbers).
4. Build the Debian package (see `from-sources/README.md`).
5. Update the SPEC files for COPR packages.
   - Update the versions.
   - Reset the `Release` back to 1 (and increment on further updates).
   - Push to GitLab.
   - Login to <https://copr.fedorainfracloud.org/coprs/d3s/main/>.
   - On the _Packages_ tab run _Rebuild_ for the corresponding packages.
     - Sometimes one of the target architectures fail, clicking _Resubmit_
       from the build page rebuilds failed targets only.
6. Rebuild Docker images after COPR finishes.
   - No changes should be needed on Docker images.
7. Check that the Docker images can be used on SIMIOS (our internal) repository.
