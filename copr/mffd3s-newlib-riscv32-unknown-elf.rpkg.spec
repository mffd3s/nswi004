%define _build_id_links none
%define __strip /opt/mffd3s/riscv32/bin/riscv32-unknown-elf-strip

Name: mffd3s-newlib-riscv32-unknown-elf
Version: 4.4.0.20231231
Release: 1%{?dist}
Summary: Cross-build newlib for riscv32-unknown-elf.

License: BSD
URL: https://www.sourceware.org/newlib/
Source: https://sourceware.org/pub/newlib/newlib-4.4.0.20231231.tar.gz

BuildRequires: mffd3s-binutils-riscv32-unknown-elf
BuildRequires: mffd3s-gcc-riscv32-unknown-elf
BuildRequires: gcc-c++
BuildRequires: texinfo

%description
Cross-build newlib for RISC-V 32.
Used at Charles University course NSWI200.


%global debug_package %{nil}

%prep
tar xzf $RPM_SOURCE_DIR/newlib-4.4.0.20231231.tar.gz

%build
mkdir build
cd build

env \
    CC_FOR_TARGET="/opt/mffd3s/riscv32/bin/riscv32-unknown-elf-gcc" \
    GCC_FOR_TARGET="/opt/mffd3s/riscv32/bin/riscv32-unknown-elf-gcc" \
    CXX_FOR_TARGET="/opt/mffd3s/riscv32/bin/riscv32-unknown-elf-g++" \
    AR_FOR_TARGET="/opt/mffd3s/riscv32/bin/riscv32-unknown-elf-ar" \
    AS_FOR_TARGET="/opt/mffd3s/riscv32/bin/riscv32-unknown-elf-as" \
    LD_FOR_TARGET="/opt/mffd3s/riscv32/bin/riscv32-unknown-elf-ld" \
    NM_FOR_TARGET="/opt/mffd3s/riscv32/bin/riscv32-unknown-elf-nm" \
    OBJCOPY_FOR_TARGET="/opt/mffd3s/riscv32/bin/riscv32-unknown-elf-objcopy" \
    OBJDUMP_FOR_TARGET="/opt/mffd3s/riscv32/bin/riscv32-unknown-elf-objdump" \
    RANLIB_FOR_TARGET="/opt/mffd3s/riscv32/bin/riscv32-unknown-elf-ranlib" \
    READELF_FOR_TARGET="/opt/mffd3s/riscv32/bin/riscv32-unknown-elf-readelf" \
    STRIP_FOR_TARGET="/opt/mffd3s/riscv32/bin/riscv32-unknown-elf-strip" \
    ../newlib-4.4.0.20231231/configure \
    --prefix=/opt/mffd3s/riscv32/ \
    --target=riscv32-unknown-elf \
    --program-prefix=riscv32-unknown-elf- \
    --disable-nls

%make_build all

%install
cd build
rm -rf $RPM_BUILD_ROOT
make install DESTDIR=$RPM_BUILD_ROOT
rm -rf $RPM_BUILD_ROOT/opt/mffd3s/riscv32/share/info/
find $RPM_BUILD_ROOT/

%files
/opt/mffd3s/riscv32/*

%changelog
