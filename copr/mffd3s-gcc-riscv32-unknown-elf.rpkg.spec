%define _build_id_links none

Name: mffd3s-gcc-riscv32-unknown-elf
Version: 14.2.0
Release: 4%{?dist}
Summary: Cross-build GCC for riscv32-unknown-elf.

License: GPL
URL: http://gcc.gnu.org
Source: https://ftpmirror.gnu.org/gcc/gcc-14.2.0/gcc-14.2.0.tar.gz

%if %{defined suse_version}
Requires: libgmp10
Requires: libisl23
Requires: libmpc3
BuildRequires: mpc-devel
%else
Requires: gmp
Requires: isl
Requires: libmpc
BuildRequires: libmpc-devel
%endif
BuildRequires: gmp-devel
BuildRequires: isl-devel
BuildRequires: gcc-c++

%description
Cross-build GCC for RISC-V 32.
Used at Charles University course NSWI200.


%global debug_package %{nil}

%prep
tar xzf $RPM_SOURCE_DIR/gcc-14.2.0.tar.gz

%build
mkdir build
cd build

# Bypass issues with format-security warnings
fixed_cflags=`echo %{optflags} | sed -e 's/-Werror=format-security/-Wformat-security/g'`

CFLAGS="$fixed_cflags" CXXFLAGS="$fixed_cflags" ../gcc-14.2.0/configure \
    --prefix=/opt/mffd3s/riscv32/ \
    --target=riscv32-unknown-elf \
    --program-prefix=riscv32-unknown-elf- \
    --with-gnu-as \
    --with-gnu-ld \
    --disable-nls \
    --with-arch=rv32ima \
    --enable-languages=c,c++ \
    --enable-lto \
    --disable-werror \
    --enable-static \
    --disable-shared \
    --without-headers

%make_build all-gcc

%install
cd build
rm -rf $RPM_BUILD_ROOT
make install-gcc DESTDIR=$RPM_BUILD_ROOT
rm -rf $RPM_BUILD_ROOT/opt/mffd3s/riscv32/share/info/
find $RPM_BUILD_ROOT/

%files
/opt/mffd3s/riscv32/*

%changelog
