%define _build_id_links none
%define __strip /opt/mffd3s/riscv32/bin/riscv32-unknown-elf-strip

Name: mffd3s-libstdc++v3-riscv32-unknown-elf
Version: 14.2.0
Release: 3%{?dist}
Summary: Cross-build libstdc++v3 for riscv32-unknown-elf.

License: GPL
URL: http://gcc.gnu.org
Source: https://ftpmirror.gnu.org/gcc/gcc-14.2.0/gcc-14.2.0.tar.gz

%if %{defined suse_version}
Requires: libgmp10
Requires: libisl15
Requires: libmpc3
BuildRequires: mpc-devel
%else
Requires: gmp
Requires: isl
Requires: libmpc
BuildRequires: libmpc-devel
%endif
BuildRequires: gmp-devel
BuildRequires: isl-devel
BuildRequires: gcc-c++
BuildRequires: mffd3s-binutils-riscv32-unknown-elf
BuildRequires: mffd3s-gcc-riscv32-unknown-elf
BuildRequires: mffd3s-newlib-riscv32-unknown-elf

%description
Cross-build libstdc++v3 for RISC-V 32.
Used at Charles University course NSWI200.


%global debug_package %{nil}

%prep
tar xzf $RPM_SOURCE_DIR/gcc-14.2.0.tar.gz

%build
mkdir build
cd build

# Bypass issues with format-security warnings
fixed_cflags=`echo %{optflags} | sed -e 's/-Werror=format-security/-Wformat-security/g'`

env \
    CC_FOR_TARGET="/opt/mffd3s/riscv32/bin/riscv32-unknown-elf-gcc" \
    GCC_FOR_TARGET="/opt/mffd3s/riscv32/bin/riscv32-unknown-elf-gcc" \
    CXX_FOR_TARGET="/opt/mffd3s/riscv32/bin/riscv32-unknown-elf-g++" \
    AR_FOR_TARGET="/opt/mffd3s/riscv32/bin/riscv32-unknown-elf-ar" \
    AS_FOR_TARGET="/opt/mffd3s/riscv32/bin/riscv32-unknown-elf-as" \
    LD_FOR_TARGET="/opt/mffd3s/riscv32/bin/riscv32-unknown-elf-ld" \
    NM_FOR_TARGET="/opt/mffd3s/riscv32/bin/riscv32-unknown-elf-nm" \
    OBJCOPY_FOR_TARGET="/opt/mffd3s/riscv32/bin/riscv32-unknown-elf-objcopy" \
    OBJDUMP_FOR_TARGET="/opt/mffd3s/riscv32/bin/riscv32-unknown-elf-objdump" \
    RANLIB_FOR_TARGET="/opt/mffd3s/riscv32/bin/riscv32-unknown-elf-ranlib" \
    READELF_FOR_TARGET="/opt/mffd3s/riscv32/bin/riscv32-unknown-elf-readelf" \
    STRIP_FOR_TARGET="/opt/mffd3s/riscv32/bin/riscv32-unknown-elf-strip" \
    CFLAGS="$fixed_cflags" \
    CXXFLAGS="$fixed_cflags" \
    ../gcc-14.2.0/configure \
    --prefix=/opt/mffd3s/riscv32/ \
    --target=riscv32-unknown-elf \
    --program-prefix=riscv32-unknown-elf- \
    --with-gnu-as \
    --with-gnu-ld \
    --disable-nls \
    --with-arch=rv32ima \
    --enable-languages=c,c++ \
    --enable-lto \
    --disable-werror \
    --enable-libstdcxx \
    --with-newlib \
    --without-headers

%make_build all-target-libstdc++-v3

%install
cd build
rm -rf $RPM_BUILD_ROOT
make install-target-libstdc++-v3 DESTDIR=$RPM_BUILD_ROOT
rm -rf $RPM_BUILD_ROOT/opt/mffd3s/riscv32/share/info/
find $RPM_BUILD_ROOT/

%files
/opt/mffd3s/riscv32/*

%changelog
