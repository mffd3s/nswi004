%define _build_id_links none
%define __strip /bin/true

Name: mffd3s-binutils-riscv32-unknown-elf
Version: 2.43
Release: 3%{?dist}
Summary: Cross-build binary utilities for riscv32-unknown-elf

License: GPL
URL: https://sourceware.org/binutils
Source: https://ftp.gnu.org/gnu/binutils/binutils-2.43.tar.bz2

%if %{defined suse_version}
Requires: libgmp10
Requires: libisl23
Requires: libmpc3
BuildRequires: mpc-devel
%else
Requires: gmp
Requires: isl
Requires: libmpc
BuildRequires: libmpc-devel
%endif
BuildRequires: gmp-devel
BuildRequires: isl-devel
BuildRequires: gcc-c++


%description
Cross-build binary utilities for RISC-V 32
Used at Charles University course NSWI200.


%global debug_package %{nil}

%prep
tar xjf $RPM_SOURCE_DIR/binutils-2.43.tar.bz2

%build
cd binutils-2.43
./configure \
  --prefix=/opt/mffd3s/riscv32/ \
  --target=riscv32-unknown-elf \
  --program-prefix=riscv32-unknown-elf- \
  --disable-nls \
  --disable-werror \
  --disable-gdb \
  --enable-gold \
  --enable-deterministic-archives \
  --enable-static \
  --with-sysroot \
  --disable-shared

%make_build all

%install
cd binutils-2.43
rm -rf $RPM_BUILD_ROOT
make install DESTDIR=$RPM_BUILD_ROOT
rm -rf $RPM_BUILD_ROOT/opt/mffd3s/riscv32/share/info/
find $RPM_BUILD_ROOT

%files
/opt/mffd3s/riscv32/*

%changelog
