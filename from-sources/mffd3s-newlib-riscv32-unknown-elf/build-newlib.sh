#!/bin/bash

set -ueo pipefail

TARGET=riscv32-unknown-elf
PREFIX="${PREFIX:-/opt/mffd3s/riscv32/}"
VERSION="newlib-4.4.0.20231231"

source "$( dirname "$0" )/../common.sh"


build_dir="$PWD/tmp-build-newlib"
tarball="$PWD/$VERSION.tar.bz2"

fetch_tarball "$tarball" "ftp://sourceware.org/pub/newlib/$VERSION.tar.gz" "b8ecda015f2ded3f0f5e9258951f767e"

if [ -e "$build_dir" ]; then
    echo "Refusing to work when $build_dir exists." >&2
    exit 2
fi

mkdir "$build_dir"
cd "$build_dir"

echo "Extracting the tarball..." >&2
tar xzf "$tarball"
env \
    CC_FOR_TARGET="$PREFIX/bin/$TARGET-gcc" \
    GCC_FOR_TARGET="$PREFIX/bin/$TARGET-gcc" \
    CXX_FOR_TARGET="$PREFIX/bin/$TARGET-g++" \
    AR_FOR_TARGET="$PREFIX/bin/$TARGET-ar" \
    AS_FOR_TARGET="$PREFIX/bin/$TARGET-as" \
    LD_FOR_TARGET="$PREFIX/bin/$TARGET-ld" \
    NM_FOR_TARGET="$PREFIX/bin/$TARGET-nm" \
    OBJCOPY_FOR_TARGET="$PREFIX/bin/$TARGET-objcopy" \
    OBJDUMP_FOR_TARGET="$PREFIX/bin/$TARGET-objdump" \
    RANLIB_FOR_TARGET="$PREFIX/bin/$TARGET-ranlib" \
    READELF_FOR_TARGET="$PREFIX/bin/$TARGET-readelf" \
    STRIP_FOR_TARGET="$PREFIX/bin/$TARGET-strip" \
    "$VERSION/configure" \
    "--target=$TARGET" \
    "--prefix=$PREFIX" \
    "--program-prefix=$TARGET-" \
    --disable-nls

make "-j$MAX_JOBS" all
make "-j$MAX_JOBS" install "DESTDIR=$build_dir/PKG"

echo "Package built inside $build_dir/PKG."

