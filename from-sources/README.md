# Building from sources

## Requirements

### Debian, Ubuntu and derivatives

```shell
apt-get update
apt-get install patchelf build-essential bzip2 wget libmpfr-dev libmpc-dev git xutils-dev libreadline-dev texinfo libncurses5-dev
```

For packaging with [FPM](https://fpm.readthedocs.io/)

```shell
apt-get install ruby ruby-dev rubygems
gem install --no-document fpm
```

### Fedora

We recommend you use our prebuilt packages from
[our Copr repository](https://copr.fedorainfracloud.org/coprs/d3s/main/).


## Building

### GCC, binutils

Each of the scripts below will create `tmp-build-*` directory with
`PKG` subdirectory that can be copied to root of your filesystem
(the only operation requiring elevated privileges).
You have to copy the **files from `PKG` subdirectory yourself**.

If you wish to install to a different directory, set `PREFIX` to the
required value.
Default is `export PREFIX=/opt/mffd3s/mips32` for MIPS and
`export PREFIX=/opt/mffd3s/riscv32` for RISC-V.

Then run the following build scripts.

```shell
cd mffd3s-binutils-mipsel-linux-gnu
./build-binutils.sh
```

```shell
cd mffd3s-gcc-mipsel-linux-gnu
./build-gcc.sh
```

Or the RISCV-32 variant.

We also offer `newlib` build for RISC-V if you need it.

### MSIM

Update the `--prefix` to match your desired system configuration.

```shell
git clone https://github.com/d-iii-s/msim.git
cd msim
./configure --prefix=/usr
make
```

As a superuser run `make install`.

## Packaging

The build scripts are accompanied by [FPM](https://fpm.readthedocs.io/)
configuration files.

Hence, simply running the following will build a Debian package
after the `build-*.sh` scripts completed.

```shell
fpm -t deb
```

Note that we only attempt to build Debian packages; other distributions
will probably work too but may miss correct dependency information.

We have used the following command to build Ubuntu packages:

```shell
mkdir packages-ubuntu-24.04
podman run \
    --rm \
    -v .:/root/src:ro \
    -v ./packages-ubuntu-24.04/:/root/packages:rw \
    -it \
    ubuntu:24.04 \
    /bin/bash -c \
       'cd /root/ && mkdir build && find src/ -maxdepth 2 -and -\( -name 'build-*.sh' -or -name '.fpm' -or -name common.sh -or -path '*/contrib/*' -\)  -exec cp --parents {} build/ \; && cd build/src && find . && ./contrib/build-debian-packages.sh && cp debian-packages/*.deb /root/packages && echo "ALL OK"'
```

