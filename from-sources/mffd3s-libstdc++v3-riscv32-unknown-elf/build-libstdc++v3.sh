#!/bin/bash

set -ueo pipefail

TARGET=riscv32-unknown-elf
PREFIX="${PREFIX:-/opt/mffd3s/riscv32/}"
VERSION="gcc-14.2.0"

source "$( dirname "$0" )/../common.sh"

build_dir="$PWD/tmp-build-libstdc++v3"
tarball="$PWD/$VERSION.tar.gz"

fetch_tarball "$tarball" "ftp://ftp.gnu.org/gnu/gcc/$VERSION/$VERSION.tar.gz" "b89ddcdaf5c1b6214abad40d9761a6ba"

if [ -e "$build_dir" ]; then
    echo "Refusing to work when $build_dir exists." >&2
    exit 2
fi

mkdir "$build_dir"
cd "$build_dir"

echo "Extracting the tarball..." >&2
tar xzf "$tarball"
env \
    CC_FOR_TARGET="$PREFIX/bin/$TARGET-gcc" \
    GCC_FOR_TARGET="$PREFIX/bin/$TARGET-gcc" \
    CXX_FOR_TARGET="$PREFIX/bin/$TARGET-g++" \
    AR_FOR_TARGET="$PREFIX/bin/$TARGET-ar" \
    AS_FOR_TARGET="$PREFIX/bin/$TARGET-as" \
    LD_FOR_TARGET="$PREFIX/bin/$TARGET-ld" \
    NM_FOR_TARGET="$PREFIX/bin/$TARGET-nm" \
    OBJCOPY_FOR_TARGET="$PREFIX/bin/$TARGET-objcopy" \
    OBJDUMP_FOR_TARGET="$PREFIX/bin/$TARGET-objdump" \
    RANLIB_FOR_TARGET="$PREFIX/bin/$TARGET-ranlib" \
    READELF_FOR_TARGET="$PREFIX/bin/$TARGET-readelf" \
    STRIP_FOR_TARGET="$PREFIX/bin/$TARGET-strip" \
    "$VERSION/configure" \
    "--target=$TARGET" \
    "--prefix=$PREFIX" \
    "--program-prefix=$TARGET-" \
    --with-gnu-as \
    --with-gnu-ld \
    --disable-nls \
    --with-arch=rv32ima \
    --enable-languages=c,c++ \
    --enable-lto \
    --disable-werror \
    --enable-libstdcxx \
    --with-newlib \
    --without-headers

make "-j$MAX_JOBS" all-target-libstdc++-v3
make "-j$MAX_JOBS" install-target-libstdc++-v3 "DESTDIR=$build_dir/PKG"

echo "Package built inside $build_dir/PKG."

