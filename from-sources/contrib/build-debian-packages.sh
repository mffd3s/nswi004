#!/bin/bash

set -ueo pipefail

echo "WARNING: experimental script, use at your own risk." >&2
echo "Hit Ctrl-C within 10 seconds to abort." >&2
sleep 10

echo "Starting..." >&2

cd "$( dirname "$0" )"

set -x

apt-get update

ln -fs /usr/share/zoneinfo/UTC /etc/localtime
env DEBIAN_FRONTEND=noninteractive apt-get install -y tzdata
dpkg-reconfigure --frontend noninteractive tzdata

apt-get install -y build-essential bzip2 wget libmpfr-dev libmpc-dev git xutils-dev libreadline-dev texinfo
apt-get install -y ruby ruby-dev rubygems
gem install --no-document fpm

cd ../mffd3s-binutils-mipsel-linux-gnu/
./build-binutils.sh
fpm -t deb -f

cd ../mffd3s-gcc-mipsel-linux-gnu/
./build-gcc.sh
fpm -t deb -f

cd ../mffd3s-binutils-riscv32-unknown-elf/
./build-binutils.sh
fpm -t deb -f

cd ../mffd3s-gcc-riscv32-unknown-elf/
./build-gcc.sh
fpm -t deb -f

cd ..
git clone https://github.com/d-iii-s/msim.git
cd msim
./contrib/build-deb.sh

cd ..
mkdir -p debian-packages
cp \
    mffd3s-binutils-mipsel-linux-gnu/*.deb \
    mffd3s-gcc-mipsel-linux-gnu/*.deb \
    mffd3s-binutils-riscv32-unknown-elf/*.deb \
    mffd3s-gcc-riscv32-unknown-elf/*.deb \
    msim/debian-package/*.deb \
    debian-packages

