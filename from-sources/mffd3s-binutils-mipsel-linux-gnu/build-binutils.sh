#!/bin/bash

set -ueo pipefail

TARGET=mipsel-linux-gnu
PREFIX="${PREFIX:-/opt/mffd3s/mips32/}"
VERSION="binutils-2.43"

source "$( dirname "$0" )/../common.sh"


build_dir="$PWD/tmp-build-binutils"
tarball="$PWD/$VERSION.tar.bz2"

fetch_tarball "$tarball" "ftp://ftp.gnu.org/gnu/binutils/$VERSION.tar.bz2" "913899817577d1d79ed18abaed3c26b4"

if [ -e "$build_dir" ]; then
    echo "Refusing to work when $build_dir exists." >&2
    exit 2
fi

mkdir "$build_dir"
cd "$build_dir"

echo "Extracting the tarball..." >&2
tar xjf "$tarball"
"$VERSION/configure" \
    "--target=$TARGET" \
    "--prefix=$PREFIX" \
    "--program-prefix=$TARGET-" \
    --disable-gdb \
    --disable-nls \
    --disable-werror \
    --enable-deterministic-archives \
    --with-sysroot

make "-j$MAX_JOBS" all
make "-j$MAX_JOBS" install "DESTDIR=$build_dir/PKG"

echo "Package built inside $build_dir/PKG."
