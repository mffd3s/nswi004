#!/bin/bash

fetch_tarball() {
    local tarball="$1"
    local url="$2"
    local checksum="$3"

    if ! [ -e "$tarball" ]; then
        if ! wget "$url" -O "$tarball"; then
            echo "wget $url failed." >&2
            exit 1
        fi
    fi
    if ! [ "$checksum" = "$( md5sum "$tarball" | cut '-d ' -f 1 )" ]; then
        echo "$tarball: invalid checksum." >&2
        exit 1
    fi
}

MAX_JOBS="$( nproc 2>/dev/null )"
if [ -z "$MAX_JOBS" ]; then
    MAX_JOBS=2
fi
export MAX_JOBS
