#!/bin/bash

set -ueo pipefail

TARGET=riscv32-unknown-elf
PREFIX="${PREFIX:-/opt/mffd3s/riscv32/}"
VERSION="gcc-14.2.0"

source "$( dirname "$0" )/../common.sh"


build_dir="$PWD/tmp-build-gcc"
tarball="$PWD/$VERSION.tar.gz"

fetch_tarball "$tarball" "ftp://ftp.gnu.org/gnu/gcc/$VERSION/$VERSION.tar.gz" "b89ddcdaf5c1b6214abad40d9761a6ba"

if [ -e "$build_dir" ]; then
    echo "Refusing to work when $build_dir exists." >&2
    exit 2
fi

mkdir "$build_dir"
cd "$build_dir"

echo "Extracting the tarball..." >&2
tar xzf "$tarball"
"$VERSION/configure" \
    "--target=$TARGET" \
    "--prefix=$PREFIX" \
    "--program-prefix=$TARGET-" \
    --with-gnu-as \
    --with-gnu-ld \
    --disable-nls \
    --with-arch=rv32ima \
    --enable-languages=c,c++ \
    --enable-lto \
    --disable-werror \
    --without-headers

make "-j$MAX_JOBS" all-gcc
make "-j$MAX_JOBS" install-gcc "DESTDIR=$build_dir/PKG"

echo "Package built inside $build_dir/PKG."
